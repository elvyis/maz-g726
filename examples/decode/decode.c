#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "maz_cpnt_g726.h"

#define MAZASSERT_RETVAL(condition, ret, fmt, msg...)                   \
    if (condition)                                                      \
    {                                                                   \
        printf(fmt "\r\n", ##msg);                                      \
        return ret;                                                     \
    }

#define MAZASSERT_RETVAL_NOMSG(condition, ret)                          \
    if (condition)                                                      \
    {                                                                   \
        return ret;                                                     \
    }

uint32_t get_file_size(char *file_name);

int main(int argc, char **argv)
{
    int ret = 0;
    int16_t ival = 0;
    uint8_t oval = 0;
    int size  = 0;
    int index = 0;
    
    HANDLE_G726CODEC handle;

    char        iname[32];
    FILE       *ifile       = NULL;
    uint8_t    *ibuf        = NULL;
    uint32_t    isize       = 0;
    int         ircnt       = 0;

    char        oname[32];
    FILE       *ofile       = NULL;
    uint8_t    *obuf        = NULL;
    uint32_t    osize       = 0;
    int         owcnt       = 0;

    /* 打印函数使用方法 */
    if (argc != 4)
    {
        printf("usage: %s <size> <g726> <pcm>\n", argv[0]);
        printf("eg   : %s 2 hello.pcm hello.g726\n", argv[0]);
        printf("\n");
        return -1;
    }

    sscanf(argv[1], "%d", &size);
    printf("size     = %d\n", size);
    MAZASSERT_RETVAL(size < 2 || size > 5, -1, "err: size out of range");

    /* 解析输入参数得到文件名 */
    strcpy(iname, argv[2]);
    strcpy(oname, argv[3]);
    printf("iname    = %s\n", iname);
    printf("oname    = %s\n", oname);

    /* 获取输入文件大小, 计算输出文件大小 */
    isize = get_file_size(iname);
    osize = isize / size * 16;
    printf("isize    = %d\n", isize);
    printf("osize    = %d\n", osize);

    /* 申请数据内存 */
    ibuf = (uint8_t *)malloc(isize);
    obuf = (uint8_t *)malloc(osize);
    memset(ibuf, 0, isize);
    memset(obuf, 0, osize);

    /* 读取输出数据 */
    ifile = fopen(iname, "r");
    MAZASSERT_RETVAL(NULL == ifile, -1, "err: can not open %s file", iname);

    ircnt = fread(ibuf, 1, isize, ifile);
    fclose(ifile);
    MAZASSERT_RETVAL(ircnt != isize, ret, "err: fread, rcnt = %d", ircnt);

    /* 音频编码 */
    handle = MAZ_CPNT_g726_open(size);
    MAZ_CPNT_g726_decode(handle, ibuf, ircnt, (short *)obuf, 0);

    /* 输出到文件 */
    ofile = fopen(oname, "wb");
    MAZASSERT_RETVAL(NULL == ofile, -1, "err: can not open %s file", oname);

    owcnt = fwrite(obuf, 1, osize, ofile);
    fclose(ofile);
    MAZASSERT_RETVAL(owcnt != osize, ret, "err: fwrite, wcnt = %d", owcnt);
    
    MAZ_CPNT_g726_close(handle);

    free(ibuf);
    free(obuf);

    printf("G.726 encode finish!\n");

    return 0;
}

uint32_t get_file_size(char *file_name)
{
    struct stat buf;
    if(stat(file_name, &buf) < 0)
    {
        return 0;
    }
    return (uint32_t)buf.st_size;
}

