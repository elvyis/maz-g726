#ifndef _MAZ_CPNT_G726_H_
#define _MAZ_CPNT_G726_H_

#define int8_t      char
#define int16_t     short
#define int32_t     int

#define uint8_t     unsigned char
#define uint16_t    unsigned short
#define uint32_t    unsigned int

/*! Bitstream handler state */
typedef struct bitstream_state_s
{
    uint32_t bitstream;
    int residue;
} bitstream_state_t;

typedef struct g726_state_s
{
    int rate;
    int bits_per_sample;
    int yl;
    int16_t yu;
    int16_t dms;
    int16_t dml;
    int16_t ap;
    int16_t a[2];
    int16_t b[6];
    int16_t pk[2];
    int16_t dq[6];
    int16_t sr[2];
    int td;
    bitstream_state_t bs;
} g726_state_t;

typedef struct g726_state_s
    *HANDLE_G726CODEC; 

HANDLE_G726CODEC MAZ_CPNT_g726_open(int bitCount);
void MAZ_CPNT_g726_decode(HANDLE_G726CODEC handle, uint8_t *inData, int len, int16_t *outData, int type);
void MAZ_CPNT_g726_encode(HANDLE_G726CODEC handle, int16_t *inData, int len, uint8_t *outData, int type);
void MAZ_CPNT_g726_close(HANDLE_G726CODEC handle);

#endif /* _MAZ_CPNT_G726_H_ */
